include_recipe 'git'

group node[:nbbapp_stack][:group]

user node[:nbbapp_stack][:user] do
  password node[:nbbapp_stack][:password]
  gid node[:nbbapp_stack][:group]
  home "/home/#{node[:nbbapp_stack][:user]}"
  supports manage_home: true
  shell "/bin/bash"
end

include_recipe "nginx"
include_recipe "nodejs"
