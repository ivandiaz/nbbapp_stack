#
# Cookbook Name:: nbbapp_stack
# Recipe:: default
#
# Copyright (C) 2013 YOUR_NAME
# 
# All rights reserved - Do Not Redistribute
#

include_recipe "nbbapp_stack::webserver"
include_recipe "nbbapp_stack::database"
include_recipe "nbbapp_stack::programming_language"
