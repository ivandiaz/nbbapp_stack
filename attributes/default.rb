default[:nbbapp_stack][:user] = "ivan"
default[:nbbapp_stack][:group] = "root"
default[:nbbapp_stack][:password] = "123"

default[:nbbapp_stack][:database][:app][:username] = 'nbbapp'
default[:nbbapp_stack][:database][:app][:password] = 'supersecret'

default[:mysql][:server_root_password] = 'rootpass'
default[:mysql][:server_debian_password] = 'debpass'
default[:mysql][:server_repl_password] = 'replpass'
default[:mysql][:host] = 'localhost'