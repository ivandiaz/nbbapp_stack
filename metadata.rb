name             'nbbapp_stack'
maintainer       'YOUR_NAME'
maintainer_email 'YOUR_EMAIL'
license          'All rights reserved'
description      'Installs/Configures nbbapp_stack'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends "nginx", 	"~> 1.8.0"
depends "mysql", 	"~> 3.0.0"
depends "nodejs", 	"~> 1.3.0"
depends "rbenv",	"~> 1.6.0"
depends "git",		"~> 2.6.0"